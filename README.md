# ctfms

The CTF Management System for speedrun-ctf.

## organisation

There are a few moving parts here; let's go over them one at a time and how they relate to the overall system for ctfms.

### central

The 'central' crate defines the central server which will be inaccessible to the user. This server manages the scoring.
It retains a list of username/secret combos, the users who have solved which challenges, and a map from flag to the
corresponding challenge.

TUI instances can submit flags, request current score of the user, and request challenge descriptions. TUI instances
will have a secret specific to the user. When a TUI instance submits a flag, the flag will be checked. If it is correct
for any challenge, then the user will be inserted into the set of solvers for that challenge. If insertion fails due to
the key already being present, then the user will be notified they have already solved it. If the insertion succeeds,
the current scoreboard will be computed for all players, with the server sending back the top 5 and the two competitors
above and below the user along with all the associated scores.

registrar instances will submit username/secret combos when a new user is created. If the user already exists, the
registrar will be notified. The registrar will have its own secret.

At _no point_ should central instances be accessible directly by the user.

### registrar

The 'registrar' crate defines the server which will process certificate signature requests (CSRs) as requested by users.
The certificates signed by this registrar will be recognised and accepted by the instancing proxy so that users can
access their own instances, without interference from other players.

Upon connection, the user will submit a name they intend to use. The registrar will check with the central server if
the user exists. If the user does not exist, the registrar will send the user the sequence of commands necessary to
generate a new SSL key and a CSR. The user will be prompted to submit this CSR to complete registration. After doing so,
the registrar will sign this CSR. If the signing succeeds, the registrar will notify the central server of the new
user/secret combo. If the user exists, the registrar will notify the process failed and to try again with a new name.
If it succeeds, the full chain certificate will be returned to the user and may be used to connect in the future. Each
request to the central server will be authenticated with a pre-defined registrar secret.

Certificates issued will expire one week after the competition ends.

The registrar will have no persistent storage.

The registrar will be accessed when connecting with 'ctf' as the SNI and without a signed client certificate.

### tui

The 'tui' crate defines the server which will be instanced per user and give access to the current status of the
competition. Each instance will hold a secret which uniquely identifies the user which will be sent to the central
server with every request.

For a user, the TUI will act as a persistent connection which can be used to submit flags, get the current score, and
request challenge descriptions. The TUI will render current scoreboard status upon submission of a flag. This instance
will only exist while a user is connected and will reset when the user disconnects.

The TUI will have no persistent storage.

The TUI will be accessed when connecting with 'ctf' as the SNI and with a signed client certificate.

### common

The 'common' crate defines the common structs for central, registrar, and the TUI. Nothing too special here. :)

## license

MIT licensed, baby. See [LICENSE](LICENSE) for details.

## contributing

We're not at this point yet. Come back later when we have more to contribute to.
