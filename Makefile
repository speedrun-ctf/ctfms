DOCKER ?= docker
DOCKER_COMPOSE ?= $(DOCKER) compose

all: build

run: build
	$(DOCKER_COMPOSE) up

build: creds base
	$(DOCKER_COMPOSE) build

base:
	$(DOCKER) build -t ctfms-base -f Dockerfile.base .

creds: multiplexer.json registrar.json targets.json
	cargo run -p cred-generator

multiplexer.json: multiplexer.sample.json
	$(warning "multiplexer.json did not exist, created with multiplexer.sample.json")
	cp multiplexer.sample.json multiplexer.json
	false

registrar.json: registrar.sample.json
	$(warning "registrar.json did not exist, created with registrar.sample.json")
	cp registrar.sample.json registrar.json
	false

targets.json: targets.sample.json
	$(warning "targets.json did not exist, created with targets.sample.json")
	cp targets.sample.json targets.json
	false