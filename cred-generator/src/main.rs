//! Credential generator. Specifically, a CA generator: one for multiplexing (identifying services
//! to users) and one for registration (identifying users to services). In a production environment,
//! you would likely want the registrar certificate signed by an actual CA, then everything else
//! signed by the multiplexer so you don't disclose challenge names.
//!
//! Two files are expected: `registrar.json` and `multiplexer.json`. These files will contiain the
//! distinguished name information for each certificate authority. For example, a Speedrun CTF
//! Registrar CA file (in `registrar.json`) might look something like this:
//!
//! ```json
//! {
//!   "c": "US",
//!   "st": "Texas",
//!   "o": "Speedrun CTF",
//!   "ou": "Speedrun CTF 2022",
//!   "cn": "Registrar"
//! }
//! ```
//!
//!  - `c` corresponds with the country code in which the competition is organised.
//!  - `st` corresponds with the state or province name in which the competition is organised.
//!  - `o` corresponds to the organisation running the competition, i.e. the organisers' team name.
//!  - `ou` corresponds to the organisational unit; we recommend the name of the specific
//! competition, e.g. "Speedrun CTF 2022".
//!  - `cn` corresponds to the common name, or the name of the service.
//!
//! All fields are mandatory. For the multiplexed services, certificates generated will use the
//! Country, State or Province, Organisation, and Organisational Unit associated with those found in
//! `multiplexer.json`. Common Name will be set to the name of the service.

use anyhow::Result;
use common::{CertificateAuthorityConfig, Target, TargetName};
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha20Rng;
use rcgen::{BasicConstraints, Certificate, CertificateParams, DnType, IsCa, KeyPair};
use std::collections::HashMap;
use std::fs::{create_dir_all, read, write, File};
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::str::FromStr;

fn generate_ca<P: AsRef<Path>>(path: P) -> Result<(KeyPair, Certificate)> {
    let path = path.as_ref();
    let stem = path
        .file_stem()
        .expect("File stem must be present.")
        .to_str()
        .expect("Must be valid UTF-8.");
    let key_path = format!("{}_ca.key", stem);
    let cert_path = format!("{}_ca.pem", stem);
    if Path::new(&cert_path).exists() && Path::new(&key_path).exists() {
        println!("Skipping {} as it already exists.", cert_path);
        let key_pair = KeyPair::from_pem(String::from_utf8(read(key_path)?)?.as_str())?;
        let params =
            CertificateParams::from_ca_cert_pem(String::from_utf8(read(cert_path)?)?.as_str())?;
        let certificate = params.self_signed(&key_pair)?;
        return Ok((key_pair, certificate));
    }
    let ca_config: CertificateAuthorityConfig =
        serde_json::from_reader(BufReader::new(File::open(path)?))?;
    let mut params = CertificateParams::new(vec![ca_config.c.clone()])?;
    for (dn, configured) in [
        (DnType::CountryName, ca_config.c),
        (DnType::StateOrProvinceName, ca_config.st),
        (DnType::OrganizationName, ca_config.o),
        (DnType::OrganizationalUnitName, ca_config.ou),
        (DnType::CommonName, ca_config.cn),
    ] {
        params.distinguished_name.push(dn, configured);
    }
    params.is_ca = IsCa::Ca(BasicConstraints::Unconstrained);
    let key_pair = KeyPair::generate()?;
    let cert = params.self_signed(&key_pair)?;
    write(cert_path, cert.pem())?;
    write(key_path, key_pair.serialize_pem())?;
    Ok((key_pair, cert))
}

fn generate_registrar() -> Result<()> {
    generate_ca("registrar.json")?;
    Ok(())
}

fn generate_multiplexer<R: Rng>(rng: &mut R) -> Result<()> {
    let (ca_key, ca_cert) = generate_ca("multiplexer.json")?;
    create_dir_all("certs")?;
    let ca_dn = &ca_cert.params().distinguished_name;
    let mut targets: HashMap<TargetName, Target> =
        serde_json::from_reader(BufReader::new(File::open("targets.json")?))?;
    let mut path = PathBuf::from_str("certs/")?;
    for (target, _) in targets.drain() {
        path.push(target.clone() + ".pem");
        if path.exists() {
            println!("Skipping {} as it already exists.", path.to_str().unwrap());
            continue;
        }

        let mut params = CertificateParams::new(vec![target.clone()])?;
        for dn_type in [
            DnType::CountryName,
            DnType::StateOrProvinceName,
            DnType::OrganizationName,
            DnType::OrganizationalUnitName,
        ] {
            params
                .distinguished_name
                .push(dn_type.clone(), ca_dn.get(&dn_type).unwrap().clone());
        }
        params.distinguished_name.push(DnType::CommonName, &target);
        let key_pair = KeyPair::generate()?;
        let cert = params.signed_by(&key_pair, &ca_cert, &ca_key)?;

        write(&path, cert.pem())?;
        path.pop();
        path.push(target + ".key");
        write(&path, key_pair.serialize_pem())?;
        path.pop();
    }
    let mut msecret = [0u8; 32];
    rng.fill_bytes(&mut msecret);
    let mut rsecret = [0u8; 32];
    rng.fill_bytes(&mut rsecret);
    write(
        "multiplexer.env",
        format!(
            "MSECRET={}\nRSECRET={}",
            hex::encode(msecret),
            hex::encode(rsecret)
        ),
    )?;
    Ok(())
}

fn main() -> Result<()> {
    let mut rng = ChaCha20Rng::from_entropy();
    generate_registrar()?;
    generate_multiplexer(&mut rng)?;
    Ok(())
}
