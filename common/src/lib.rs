use bollard_stubs::models::HostConfig;
use std::collections::{HashMap, HashSet};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::str::FromStr;

use regex::Regex;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use spin::Lazy;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

pub use error::{CentralError, UserError};

mod error;

pub type RequestResult<T> = Result<T, CentralError>;
pub type UserResult<T> = Result<T, UserError>;

pub type Flag = String;
pub type ImageName = String;
pub type TargetName = String;
pub type Secret = [u8; 32];

static USER_REGEX: Lazy<Regex> = Lazy::new(|| Regex::new(r"[A-Za-z\d _-]{2,32}").unwrap());

#[derive(Deserialize, Serialize, Debug, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Username(String);

impl Username {
    pub fn get_name(&self) -> &str {
        &self.0
    }

    pub fn check(&self) -> bool {
        USER_REGEX.is_match(self.get_name())
    }
}

impl FromStr for Username {
    type Err = UserError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        USER_REGEX
            .is_match(s)
            .then(|| Username(s.to_string()))
            .ok_or(UserError::IllegalUsername)
    }
}

impl AsRef<[u8]> for Username {
    fn as_ref(&self) -> &[u8] {
        self.0.as_bytes()
    }
}

pub type ChallengeSolves = HashSet<Username>;
pub type SolveList = HashMap<TargetName, ChallengeSolves>;

#[derive(Deserialize, Serialize, Debug)]
pub struct UserRegistration {
    username: Username,
    secret: Secret,
}

impl UserRegistration {
    pub fn new(username: Username, secret: Secret) -> UserRegistration {
        UserRegistration { username, secret }
    }

    pub fn username(&self) -> &Username {
        &self.username
    }

    pub fn secret(&self) -> [u8; 32] {
        self.secret
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub enum Request {
    // tui requests
    TargetList(Username, Secret),
    Score(Username, Secret),
    Target(Username, TargetName, Secret),
    FlagSubmission(Username, Flag, Secret),
    // registrar requests
    Registration(UserRegistration, Secret),
    // multiplexer requests
    ConnectionDetail(String, Secret),
    UserSecret(Username, Secret),
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Target {
    #[serde(default, skip_serializing)]
    flag: Option<Flag>,
    description: String,
    #[serde(default, skip_serializing)]
    details: Option<ConnectionDetails>,
    #[serde(default, skip_serializing)]
    hidden: bool,
}

impl Target {
    pub fn check_flag(&self, possible: Flag) -> bool {
        Some(possible) == self.flag
    }

    pub fn description(&self) -> &str {
        &self.description
    }

    pub fn details(&self) -> Option<&ConnectionDetails> {
        self.details.as_ref()
    }

    pub fn hidden(&self) -> bool {
        self.hidden
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub enum ConnectionDetails {
    Instanced(ContainerConfig),
    Fixed(SocketAddr),
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct ContainerConfig {
    pub image: ImageName,
    pub port: u16,
    #[serde(default)]
    pub binds: HashMap<PathBuf, PathBuf>,
    #[serde(default)]
    pub attached_internal: bool,
    #[serde(default)]
    pub given_registrar_secret: bool,
    #[serde(default)]
    pub given_user_secret: bool,
    #[serde(default)]
    pub unauth: bool,
    #[serde(default)]
    pub hostconfig: HostConfig,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct CertificateAuthorityConfig {
    pub c: String,
    pub st: String,
    pub o: String,
    pub ou: String,
    pub cn: String,
}

pub async fn send_message<W: AsyncWrite + AsyncWriteExt + Unpin, T: Serialize>(
    conn: &mut W,
    msg: &T,
) -> anyhow::Result<usize> {
    let serialised = serde_json::to_vec(msg)?;
    conn.write_u16(serialised.len().try_into()?).await?;
    conn.write_all(&serialised).await?;
    conn.flush().await?;
    Ok(serialised.len())
}

pub async fn await_message<R: AsyncRead + AsyncReadExt + Unpin, T: DeserializeOwned>(
    conn: &mut R,
) -> anyhow::Result<T> {
    let size = conn.read_u16().await?;
    let mut serialised = vec![0u8; size as usize];
    conn.read_exact(&mut serialised).await?;
    let deser = serde_json::from_slice(serialised.as_slice())?;
    Ok(deser)
}
