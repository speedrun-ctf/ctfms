use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Error, Debug, Deserialize, Serialize, Copy, Clone)]
pub enum CentralError {
    #[error("Bad authentication.")]
    BadAuthentication,
    #[error("User exists.")]
    UserExists,
    #[error("User does not exist.")]
    NoSuchUser,
    #[error("The challenge requested does not exist.")]
    NoSuchChallenge,
    #[error("There was an error with the user.")]
    UserError(UserError),
}

#[derive(Error, Debug, Deserialize, Serialize, Copy, Clone)]
pub enum UserError {
    #[error(r"Illegal username; must match the regex [A-Za-z\d _-]{{2,32}}")]
    IllegalUsername,
}
