#![forbid(unused_must_use)]
#![forbid(unused_results)]

use std::cell::RefCell;
use std::collections::HashMap;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::Arc;

use log::*;
use rand::RngCore;
use rand::SeedableRng;
use rand_chacha::ChaChaRng;
use sled::Db;
use tokio::io::{BufReader, BufWriter};
use tokio::net::{TcpListener, TcpStream};

thread_local! {
    static PWGEN: RefCell<ChaChaRng> = RefCell::new(ChaChaRng::from_entropy());
}

use common::{
    await_message, send_message, CentralError, ConnectionDetails, Request, Secret, Target,
    TargetName, UserError, Username,
};

const DEFAULT_CHALS: &str = "/targets.json";
const DEFAULT_PORT: u16 = 1110;
const DEFAULT_DB: &str = "/db";

fn default_saddr() -> IpAddr {
    IpAddr::V4(Ipv4Addr::from([0, 0, 0, 0]))
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    let multiplexer_secret = hex::decode(
        std::env::var("MSECRET")
            .expect("Multiplexer secret (MSECRET) required for responding to multiplexer."),
    )?
    .as_slice()
    .try_into()?;
    let registrar_secret = hex::decode(
        std::env::var("RSECRET")
            .expect("Registrar secret (RSECRET) required for responding to multiplexer."),
    )?
    .as_slice()
    .try_into()?;

    let chals = match std::env::var("TARGETS") {
        Ok(user_provided) => PathBuf::from_str(&user_provided)?,
        Err(_) => {
            warn!(
                target: "startup",
                "No targets (TARGETS) env variable specified; defaulting to {}.",
                DEFAULT_CHALS
            );
            PathBuf::from_str(DEFAULT_CHALS)?
        }
    };
    let port = match std::env::var("PORT") {
        Ok(user_provided) => u16::from_str(&user_provided)?,
        Err(_) => {
            warn!(
                target: "startup",
                "No port (PORT) env variable specified; defaulting to {}.",
                DEFAULT_PORT
            );
            DEFAULT_PORT
        }
    };
    let addr = match std::env::var("SADDR") {
        Ok(user_provided) => SocketAddr::new(IpAddr::from_str(&user_provided)?, port),
        Err(_) => {
            let default = default_saddr();
            info!(
                target: "startup",
                "No server address (SADDR) env variable specified; defaulting to {}",
                default
            );
            SocketAddr::new(default, port)
        }
    };
    let db = match std::env::var("DB") {
        Ok(user_provided) => sled::open(&user_provided)?,
        Err(_) => {
            info!(
                target: "startup",
                "No database (DB) env variable specified; defaulting to {}",
                DEFAULT_DB
            );
            sled::open(DEFAULT_DB)?
        }
    };

    let targets: HashMap<TargetName, Target> =
        serde_json::from_reader(std::io::BufReader::new(std::fs::File::open(chals)?))?;
    targets.values().for_each(|target| {
        if let Some(details) = target.details() {
            if let ConnectionDetails::Instanced(config) = details {
                assert!(!(config.unauth && config.given_user_secret));
            }
        } else {
            panic!("All boxes require connection details.");
        }
    });
    let targets = Arc::new(targets);

    let server = TcpListener::bind(addr).await?;
    info!(target: "startup", "Started listening on {}", addr);

    while let Ok((conn, addr)) = server.accept().await {
        let db = db.clone();
        let targets = targets.clone();
        info!(target: "server", "Accepted connection from {}", addr);
        let _ = tokio::spawn(async move {
            handle_connection(registrar_secret, multiplexer_secret, targets, conn, db).await
        });
    }

    Ok(())
}

fn check_user_credentials(db: &Db, user: &Username, secret: &Secret) -> anyhow::Result<bool> {
    Ok(db
        .open_tree("users")?
        .get(user)?
        .map_or(false, |expected| expected.as_ref() == secret))
}

async fn handle_connection(
    registrar_secret: Secret,
    multiplexer_secret: Secret,
    targets: Arc<HashMap<TargetName, Target>>,
    mut conn: TcpStream,
    db: Db,
) -> anyhow::Result<()> {
    let (rx, tx) = conn.split();
    let mut reader = BufReader::new(rx);
    let mut writer = BufWriter::new(tx);

    let req = await_message::<_, Request>(&mut reader).await?;

    match req {
        Request::Registration(reg, sec) if sec == registrar_secret => {
            info!(target: "registrar", "Attempting to register user {}", reg.username().get_name());
            let res = if !reg.username().check() {
                Err(CentralError::UserError(UserError::IllegalUsername))
            } else {
                let user_secret = PWGEN.try_with(|pwgen| {
                    let mut secret = Secret::default();
                    pwgen.borrow_mut().fill_bytes(&mut secret);
                    secret
                })?;
                if db
                    .open_tree("users")?
                    .compare_and_swap::<_, &Secret, _>(reg.username(), None, Some(&user_secret))?
                    .is_err()
                {
                    Err(CentralError::UserExists)
                } else {
                    info!(target: "registrar", "Registered user {}", reg.username().get_name());
                    Ok(())
                }
            };
            debug!(target: "multiplexer", "Performed registration; got: {:?}", res);
            let _ = send_message(&mut writer, &res).await?;
            res
        }
        Request::ConnectionDetail(sni, sec) if sec == multiplexer_secret => {
            info!(target: "multiplexer", "Attempting to fetch connection details for {}", sni);
            let res = if let Some(target) = targets.get(&sni) {
                Ok(target
                    .details()
                    .expect("This should've been checked earlier..."))
            } else {
                Err(CentralError::NoSuchChallenge)
            };
            debug!(target: "multiplexer", "Fetched challenge; got: {:?}", res);
            let n = send_message(&mut writer, &res).await?;
            debug!(target: "multiplexer", "Sent {} bytes back.", n);
            res.map(|_| ())
        }
        Request::UserSecret(user, sec) if sec == multiplexer_secret => {
            info!(target: "multiplexer", "Attempting to fetch user secret for {}", user.get_name());
            let res = if let Some(secret) = db.open_tree("users")?.get(user.get_name())? {
                let secret = secret.to_vec();
                let secret: [u8; 32] = secret
                    .try_into()
                    .expect("Secret should be saved with the correct length.");
                Ok(secret)
            } else {
                error!(target: "multiplexer", "User {} did not have a secret, but must have had SNI to get here.", user.get_name());
                Err(CentralError::NoSuchUser)
            };
            let _ = send_message(&mut writer, &res).await?;
            res.map(|_| ())
        }
        Request::TargetList(user, secret) if check_user_credentials(&db, &user, &secret)? => {
            unimplemented!()
        }
        Request::Score(user, secret) if check_user_credentials(&db, &user, &secret)? => {
            unimplemented!()
        }
        Request::Target(user, _target, secret) if check_user_credentials(&db, &user, &secret)? => {
            unimplemented!()
        }
        Request::FlagSubmission(user, _flag, secret)
            if check_user_credentials(&db, &user, &secret)? =>
        {
            unimplemented!()
        }
        _ => {
            let e = CentralError::BadAuthentication;
            let _ = send_message(&mut writer, &Result::<(), _>::Err(e)).await?;
            Err(e)
        }
    }?;

    Ok(())
}
