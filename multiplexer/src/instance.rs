use std::collections::HashMap;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::ops::{Deref, DerefMut};
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use anyhow::Result;
use bollard::container::{CreateContainerOptions, RemoveContainerOptions};
use bollard::models::{
    EndpointSettings, Mount, MountTypeEnum, RestartPolicy, RestartPolicyNameEnum,
};
use bollard::network::{ConnectNetworkOptions, CreateNetworkOptions, DisconnectNetworkOptions};
use bollard::service::HostConfigLogConfig;
use bollard::{container, Docker};
use log::info;
use tokio::net::TcpStream;
use tokio::sync::{Mutex, RwLock};
use tokio::task::JoinHandle;
use tokio::time::sleep;

use common::{
    await_message, send_message, ContainerConfig, ImageName, Request, RequestResult, Secret,
    Username,
};

#[derive(Clone, Ord, PartialOrd, Eq, PartialEq, Debug, Hash)]
struct UserInstance(Option<Username>, ImageName, String);

const ENDPOINT_NAME: &str = "multiplexed";

impl UserInstance {
    fn user(&self) -> Option<&Username> {
        self.0.as_ref()
    }

    fn image(&self) -> &ImageName {
        &self.1
    }

    fn container(&self) -> String {
        if let Some(user) = self.user() {
            format!("{}-{}", hex::encode(user.get_name()), self.2)
        } else {
            self.2.clone()
        }
    }

    fn network(&self) -> String {
        if let Some(user) = self.user() {
            format!("{}-{}-network", hex::encode(user.get_name()), self.2)
        } else {
            format!("{}-network", self.2)
        }
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Debug)]
pub enum InstanceOp {
    AddSession,
    RemoveSession,
}

pub struct InstanceManager {
    sessions: Arc<RwLock<HashMap<UserInstance, Arc<RwLock<u64>>>>>,
    networks: Arc<Mutex<HashMap<UserInstance, String>>>,
    containers: Arc<Mutex<HashMap<UserInstance, String>>>,
    sown: Arc<Mutex<HashMap<UserInstance, JoinHandle<()>>>>,
    docker: Docker,
}

impl InstanceManager {
    pub fn new() -> Result<Self> {
        Ok(Self {
            sessions: Default::default(),
            networks: Default::default(),
            containers: Default::default(),
            sown: Default::default(),
            docker: Docker::connect_with_socket_defaults()?,
        })
    }

    pub async fn modify_instance(
        &self,
        self_instance: &str,
        secret: Secret,
        caddr: SocketAddr,
        registrar_secret: Secret,
        internal_net: &str,
        target: String,
        user: Option<Username>,
        config: ContainerConfig,
        op: InstanceOp,
    ) -> Result<Option<SocketAddr>> {
        let instance = UserInstance(user.clone(), config.image, target);
        let read_l = {
            let initial = self.sessions.read().await;
            if !initial.contains_key(&instance) {
                drop(initial);
                let mut write_l = self.sessions.write().await;
                let entry = write_l.entry(instance.clone());
                let _ = entry.or_default();
                write_l.downgrade()
            } else {
                initial
            }
        };
        let counter_l = read_l
            .deref()
            .get(&instance)
            .expect("Value guaranteed to be present.")
            .clone();
        let mut counter = counter_l.write().await;
        let inner = counter.deref_mut();
        match op {
            InstanceOp::AddSession => {
                let container_id = if *inner == 0 {
                    (async move {
                        let result: Result<String> = {
                            let handle = self.sown.lock().await.remove(&instance);
                            if let Some(handle) = handle {
                                handle.abort();
                                let awaited = handle.await;
                                if let Err(e) = awaited {
                                    if e.is_cancelled() {
                                        return Ok(self
                                            .containers
                                            .lock()
                                            .await
                                            .get(&instance)
                                            .expect("Container must have already been spawned.")
                                            .clone());
                                    }
                                }
                            }

                            info!(target: "instancing", "Creating {} for {:?}", instance.image(), instance.user().map(|uname| uname.get_name()));
                            // spawn the network
                            let network = instance.network();
                            let network_options = CreateNetworkOptions {
                                name: network.as_str(),
                                check_duplicate: false,
                                driver: "bridge",
                                internal: true,
                                attachable: true,
                                ..Default::default()
                            };
                            let network_id = self
                                .docker
                                .create_network(network_options)
                                .await?
                                .id
                                .expect("If the network was created, it should have an ID.");

                            // spawn the container
                            let container = instance.container();
                            let container_options = CreateContainerOptions {
                                name: container.as_str(),
                                platform: None,
                            };
                            let container_config = container::Config {
                                env: {
                                    let mut env = Vec::new();
                                    if config.given_registrar_secret {
                                        env.push(format!("RSECRET={}", hex::encode(registrar_secret)));
                                        env.push(format!("CPORT={}", caddr.port()));
                                    }
                                    if config.given_user_secret {
                                        let user_secret = {
                                            let mut conn = TcpStream::connect(caddr).await?;
                                            let _ = send_message(
                                                &mut conn,
                                                &Request::UserSecret(user.expect("We can only give user secrets for authenticated boxes."), secret),
                                            )
                                                .await?;
                                            await_message::<_, RequestResult<Secret>>(&mut conn)
                                                .await??
                                        };
                                        env.push(format!("USER_SECRET={}", hex::encode(user_secret)));
                                    }
                                    if env.is_empty() {
                                        None
                                    } else {
                                        Some(env)
                                    }
                                },
                                healthcheck: None, // TODO add a health check
                                image: Some(instance.image().clone()),
                                networking_config: Some(container::NetworkingConfig {
                                    endpoints_config: {
                                        let mut endpoints = HashMap::new();
                                        let _ = endpoints.insert(
                                            ENDPOINT_NAME.to_string(),
                                            EndpointSettings {
                                                network_id: Some(network_id.clone()),
                                                ..Default::default()
                                            },
                                        );
                                        endpoints
                                    },
                                }),
                                host_config: {
                                    let mut mounts = Vec::new();
                                    for (source, target) in config.binds {
                                        mounts.push(Mount {
                                            target: target.to_str().map(|s| s.to_string()),
                                            source: source.to_str().map(|s| s.to_string()),
                                            typ: Some(MountTypeEnum::BIND),
                                            read_only: Some(true),
                                            bind_options: None,
                                            ..Default::default()
                                        });
                                    }
                                    let mut host_config = config.hostconfig;

                                    // safe default: always restart
                                    if host_config.restart_policy.is_none() {
                                        host_config.restart_policy = Some(RestartPolicy {
                                            name: Some(RestartPolicyNameEnum::ALWAYS),
                                            ..Default::default()
                                        });
                                    }
                                    // safe default: never log
                                    if host_config.log_config.is_none() {
                                        host_config.log_config = Some(HostConfigLogConfig {
                                            typ: Some("none".to_string()),
                                            ..Default::default()
                                        });
                                    }
                                    // safe default: no ipc
                                    if host_config.ipc_mode.is_none() {
                                        host_config.ipc_mode = Some("none".to_string());
                                    }
                                    // safe default: extend existing if present
                                    if mounts.is_empty() {
                                        Some(host_config)
                                    } else {
                                        if let Some(m) = &mut host_config.mounts {
                                            m.extend(mounts);
                                        } else {
                                            host_config.mounts = Some(mounts);
                                        }
                                        Some(host_config)
                                    }
                                },
                                ..Default::default()
                            };
                            let container_id = self
                                .docker
                                .create_container(Some(container_options), container_config)
                                .await?
                                .id;
                            self.docker
                                .start_container::<&str>(&container_id, None)
                                .await?;

                            if config.attached_internal {
                                let inetwork = self
                                    .docker
                                    .inspect_network::<&str>(internal_net, None)
                                    .await?;
                                self.docker
                                    .connect_network(
                                        &inetwork.id.expect("ID must be present"),
                                        ConnectNetworkOptions {
                                            container: container_id.clone(),
                                            endpoint_config: Default::default(),
                                        },
                                    )
                                    .await?;
                            }
                            self.docker
                                .connect_network(
                                    &network_id,
                                    ConnectNetworkOptions {
                                        container: self_instance,
                                        endpoint_config: Default::default(),
                                    },
                                )
                                .await?;

                            info!(target: "instancing", "Created {} for {:?}: {}", instance.image(), instance.user().map(|uname| uname.get_name()), container_id);
                            assert!(self
                                .networks
                                .lock()
                                .await
                                .insert(instance.clone(), network_id)
                                .is_none());
                            assert!(self
                                .containers
                                .lock()
                                .await
                                .insert(instance, container_id.clone())
                                .is_none());

                            Ok(container_id)
                        };
                        result
                    }).await?
                } else {
                    self.containers
                        .lock()
                        .await
                        .get(&instance)
                        .expect("Container must have already been spawned.")
                        .clone()
                };
                *inner += 1;
                let ip = self
                    .docker
                    .inspect_container(container_id.as_str(), None)
                    .await?
                    .network_settings
                    .expect("Must have network settings present.")
                    .networks
                    .expect("Must have a network present.")
                    .get(ENDPOINT_NAME)
                    .expect("At least one network must be present.")
                    .ip_address
                    .as_ref()
                    .expect("IP address must be present.")
                    .clone();
                Ok(Some(SocketAddr::new(
                    IpAddr::from(Ipv4Addr::from_str(&ip).expect("Must be a valid IP.")),
                    config.port,
                )))
            }
            InstanceOp::RemoveSession => {
                *inner -= 1;
                if inner == &0 {
                    let containers = self.containers.clone();
                    let networks = self.networks.clone();
                    let self_instance = self_instance.to_string();
                    let docker = self.docker.clone();
                    assert!(self.sown.lock().await.insert(instance.clone(), tokio::spawn(async move {
                        sleep(Duration::from_secs(60)).await;

                        // prevent the cancellation by spawning it all at once
                        let _ = tokio::spawn(async move {
                            info!(target: "instancing", "Pruning {} from the network.", instance.container());
                            let result: Result<()> = {
                                docker
                                    .remove_container(
                                        containers
                                            .lock()
                                            .await
                                            .remove(&instance)
                                            .expect(
                                                "We should have this container if there was already a session.",
                                            )
                                            .as_str(),
                                        Some(RemoveContainerOptions {
                                            force: true,
                                            ..Default::default()
                                        }),
                                    )
                                    .await?;
                                let network_id = networks
                                    .lock()
                                    .await
                                    .remove(&instance)
                                    .expect("We should have this network if there was already a session.");
                                docker
                                    .disconnect_network(
                                        network_id.as_str(),
                                        DisconnectNetworkOptions {
                                            container: self_instance,
                                            force: true,
                                        },
                                    )
                                    .await?;
                                docker.remove_network(network_id.as_str()).await?;
                                info!(target: "instancing", "{} pruned.", instance.container());
                                Ok(())
                            };
                            result
                        });
                    })).is_none());
                }
                Ok(None)
            }
        }
    }
}
