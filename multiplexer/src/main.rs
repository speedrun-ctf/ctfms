#![feature(iterator_try_collect)]
#![forbid(unused_must_use)]
#![forbid(unused_results)]
#![allow(clippy::too_many_arguments)]

use std::ffi::OsStr;
use std::fs::File;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::ops::DerefMut;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use gethostname::gethostname;
use log::{debug, error, info, warn, LevelFilter};
use rustls::crypto::aws_lc_rs::sign::any_supported_type;
use rustls::pki_types::PrivateKeyDer;
use rustls::server::{ResolvesServerCertUsingSni, WebPkiClientVerifier};
use rustls::sign::CertifiedKey;
use rustls::RootCertStore;
use rustls::{self, ServerConnection};
use rustls_pemfile::{certs, pkcs8_private_keys};
use tokio::fs::read_dir;
use tokio::io::{copy, split, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::time::sleep;
use tokio_rustls::server::TlsStream;
use tokio_rustls::TlsAcceptor;
use trust_dns_resolver::AsyncResolver;
use x509_parser::prelude::*;

use common::{
    await_message, send_message, ConnectionDetails, Request, RequestResult, Secret, Username,
};

use crate::instance::{InstanceManager, InstanceOp};

mod instance;

const DEFAULT_PORT: u16 = 443;
const DEFAULT_CERT_PATH: &str = "/certs";
const DEFAULT_CA_PATH: &str = "/ca";
const DEFAULT_INTERNAL_NET: &str = "ctfms_management";

const MAX_TRIES: usize = 10;
const RETRY_DELAY: Duration = Duration::from_millis(1000);

fn default_saddr() -> IpAddr {
    IpAddr::V4(Ipv4Addr::from([0, 0, 0, 0]))
}

async fn load_identities(cert_path: &Path) -> anyhow::Result<ResolvesServerCertUsingSni> {
    let mut certdir = read_dir(cert_path).await?;
    let mut cert_resolver = ResolvesServerCertUsingSni::new();
    while let Some(cert) = certdir.next_entry().await? {
        let path = cert.path();
        if path.extension() == Some(OsStr::new("pem")) {
            // using standard bufreader here for cert streaming
            let certfile = File::open(path.as_path())?;
            let mut certbuf = std::io::BufReader::new(certfile);
            let chain = certs(&mut certbuf).try_collect::<Vec<_>>()?;

            let stem = path
                .file_stem()
                .expect("If there's an extension, there must be a stem!")
                .to_os_string()
                .into_string()
                .expect("Should be valid UTF-8 if it's a valid SNI.");
            let mut path = cert_path.to_path_buf();
            path.push(stem.clone() + ".key");
            let keyfile = File::open(path)?;
            let mut keybuf = std::io::BufReader::new(keyfile);
            let key = any_supported_type(&PrivateKeyDer::try_from(
                pkcs8_private_keys(&mut keybuf)
                    .next()
                    .unwrap_or_else(|| panic!("Expected a {}.key", stem.as_str()))?,
            )?)
            .expect("Private key should be a supported type.");
            if !chain.is_empty() {
                cert_resolver.add(stem.as_str(), CertifiedKey::new(chain, key))?;
            }
        }
    }

    Ok(cert_resolver)
}

fn load_ca(ca_path: &Path) -> anyhow::Result<RootCertStore> {
    let mut root_store = RootCertStore::empty();
    let cafile = File::open(ca_path)?;
    let mut cabuf = std::io::BufReader::new(cafile);

    root_store.add(
        certs(&mut cabuf)
            .next()
            .ok_or_else(|| anyhow::Error::msg("No CA cert found"))??,
    )?;

    Ok(root_store)
}

async fn determine_target(
    caddr: SocketAddr,
    secret: Secret,
    addr: SocketAddr,
    connection: &ServerConnection,
) -> anyhow::Result<(Option<Username>, ConnectionDetails, String)> {
    let sni = connection.server_name().expect("SNI guaranteed");
    let details = {
        let mut conn = TcpStream::connect(caddr).await?;
        let _ = send_message(
            &mut conn,
            &Request::ConnectionDetail(sni.to_string(), secret),
        )
        .await?;
        await_message::<_, RequestResult<ConnectionDetails>>(&mut conn).await??
    };
    match connection.peer_certificates() {
        Some(certs) => {
            let (_rem, parsed) = X509Certificate::from_der(certs[0].as_ref())?;
            let mut cn = parsed.subject.iter_common_name();
            let name = cn
                .next()
                .ok_or_else(|| anyhow::Error::msg("No CN provided"))?
                .as_str()?;
            info!(r#"Peer {} ("{}") requested {}."#, addr, name, sni);
            Ok((Some(Username::from_str(name)?), details, sni.to_string()))
        }
        None => {
            info!(
                "Peer {} requested {} without a client certificate.",
                addr, sni
            );
            Ok((None, details, sni.to_string()))
        }
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    let secret: [u8; 32] = hex::decode(
        std::env::var("MSECRET")
            .expect("Multiplexer secret (MSECRET) required for connecting to the central server."),
    )?
    .as_slice()
    .try_into()?;
    let registrar_secret = hex::decode(
        std::env::var("RSECRET")
            .expect("Registrar secret (RSECRET) required for connecting to the central server."),
    )?
    .as_slice()
    .try_into()?;

    let central_addr = {
        let resolver = AsyncResolver::tokio_from_system_conf()?;
        resolver
            .lookup_ip("central")
            .await?
            .iter()
            .next()
            .expect("No IP found for central; failing fast.")
    };
    let central_port = u16::from_str(
        &std::env::var("CPORT")
            .expect("Central port (CPORT) required for connecting to the central server."),
    )
    .expect("Invalid CPORT.");
    let central = SocketAddr::new(central_addr, central_port);

    let port = match std::env::var("PORT") {
        Ok(user_provided) => u16::from_str(&user_provided)?,
        Err(_) => {
            warn!(
                target: "startup",
                "No port (PORT) env variable specified; defaulting to {}.",
                DEFAULT_PORT
            );
            DEFAULT_PORT
        }
    };
    let addr = match std::env::var("SADDR") {
        Ok(user_provided) => SocketAddr::new(IpAddr::from_str(&user_provided)?, port),
        Err(_) => {
            let default = default_saddr();
            info!(
                target: "startup",
                "No server address (SADDR) env variable specified; defaulting to {}",
                default
            );
            SocketAddr::new(default, port)
        }
    };
    let certs = PathBuf::from(std::env::var("CERTS").unwrap_or_else(|_| {
        info!(
            target: "startup",
            "No certs (CERTS) env variable specified; defaulting to {}",
            DEFAULT_CERT_PATH
        );
        String::from(DEFAULT_CERT_PATH)
    }));
    let ca = PathBuf::from(std::env::var("CA").unwrap_or_else(|_| {
        info!(
            target: "startup",
            "No ca (CA) env variable specified; defaulting to {}",
            DEFAULT_CA_PATH
        );
        String::from(DEFAULT_CA_PATH)
    }));
    let internal_net = Arc::new(std::env::var("INTERNAL_NET").unwrap_or_else(|_| {
        info!(
            target: "startup",
            "No internal network (INTERNAL_NET) env variable specified; defaulting to {}",
            DEFAULT_INTERNAL_NET
        );
        String::from(DEFAULT_INTERNAL_NET)
    }));

    // hack: hostname is the short name of the current container
    let raw_hostname = gethostname();
    let hostname = Arc::new(
        raw_hostname
            .into_string()
            .expect("Must be a valid hostname."),
    );

    let cert_resolver = Arc::new(load_identities(certs.as_path()).await?);

    let roots = Arc::new(load_ca(ca.as_path())?);

    // TODO we need to make absolutely sure they can't do fuckery with bad CAs
    let config = rustls::ServerConfig::builder()
        .with_client_cert_verifier(
            WebPkiClientVerifier::builder(roots)
                .allow_unauthenticated()
                .build()?,
        )
        .with_cert_resolver(cert_resolver);
    let acceptor = TlsAcceptor::from(Arc::new(config));

    let listener = TcpListener::bind(&addr).await?;

    let instance_manager = Arc::new(InstanceManager::new()?);

    loop {
        let (stream, addr) = listener.accept().await?;
        let acceptor = acceptor.clone();
        let instance_manager = instance_manager.clone();
        let hostname = hostname.clone();
        let internal_net = internal_net.clone();

        let _ = tokio::spawn(async move {
            let stream = Arc::new(spin::Mutex::new(acceptor.accept(stream).await?));
            let cloned = stream.clone();

            let res = (async move {
                let mut stream = cloned.lock();
                match determine_target(central, secret, addr, stream.get_ref().1).await {
                    Ok(target) => match target {
                        (_, ConnectionDetails::Fixed(target_addr), target) => {
                            // permit unauth to fixed
                            info!(target: "multiplexer", "Routing {} to {} ({})", addr, target, target_addr);
                            transfer(&mut stream, target_addr).await
                        }
                        (mut user, ConnectionDetails::Instanced(config), target)
                        if (user.is_none() && config.unauth) || user.is_some() =>
                            {
                                if config.unauth {
                                    user = None; // clear creds for unauth
                                }
                                info!(target: "multiplexer", "Routing {} ({:?}) to an instanced {}.", addr, user, target);
                                let server_addr = instance_manager
                                    .modify_instance(
                                        hostname.as_str(),
                                        secret,
                                        central,
                                        registrar_secret,
                                        internal_net.as_str(),
                                        target.clone(),
                                        user.clone(),
                                        config.clone(),
                                        InstanceOp::AddSession,
                                    )
                                    .await?
                                    .expect("Expected a socket address to be provided.");
                                let res = transfer(&mut stream, server_addr).await;
                                let _ = instance_manager
                                    .modify_instance(
                                        hostname.as_str(),
                                        secret,
                                        central,
                                        Secret::default(),
                                        internal_net.as_str(),
                                        target,
                                        user,
                                        config,
                                        InstanceOp::RemoveSession,
                                    )
                                    .await?;
                                res
                            }
                        (user, ConnectionDetails::Instanced(config), target) => {
                            debug!(target: "multiplexer", "Denied connection to {} for {} ({:?})", target, addr, user);
                            let (_, mut wi) = split(stream.deref_mut());
                            wi.write_all(
                                format!("This machine is marked as only accessible {} using a client certificate; please reconnect with the appropriate configuration.\n", if config.unauth { "when not" } else { "when" })
                                    .as_bytes(),
                            )
                                .await?;
                            wi.flush().await?;
                            Ok(())
                        }
                    },
                    Err(e) => {
                        warn!(target: "multiplexer", "Couldn't determine a target for {}: {}", stream.get_ref().1.server_name().expect("SNI guaranteed"), e);
                        let (_, mut wi) = split(stream.deref_mut());
                        let _ = wi.write_all(
                            format!("Encountered an error while determining target: {}\n", e)
                                .as_bytes(),
                        )
                            .await;
                        let _ = wi.flush().await;
                        Ok(())
                    }
                }
            }).await;

            if let Err(e) = res {
                error!(target: "server", "{:?}", e);
                let mut locked = stream.lock();
                let (_, mut wi) = split(locked.deref_mut());
                wi.write_all(
                    format!("Encountered an error while connecting you: {}\n", e).as_bytes(),
                )
                .await?;
                wi.flush().await?;
            }

            anyhow::Ok(())
        });
    }
}

async fn transfer(
    inbound: &mut TlsStream<TcpStream>,
    proxy_addr: SocketAddr,
) -> anyhow::Result<()> {
    let mut tries = 0;
    let mut outbound = loop {
        match TcpStream::connect(proxy_addr).await {
            Ok(outbound) => break outbound,
            Err(e) => match e.raw_os_error() {
                Some(111) => {
                    if tries < MAX_TRIES {
                        tries += 1;
                        info!(target: "multiplexer", "Couldn't reach {}; trying again in {} seconds.", proxy_addr, RETRY_DELAY.as_secs_f64());
                        sleep(RETRY_DELAY).await;
                    } else {
                        return Err(anyhow::Error::msg("Couldn't connect to instance. If you've just connected for the first time in a while, it's likely that the container is still starting."));
                    }
                }
                _ => return Err(e.into()),
            },
        };
    };

    let (mut ri, mut wi) = split(inbound);
    let (mut ro, mut wo) = outbound.split();

    let client_to_server = async {
        let _ = copy(&mut ri, &mut wo).await?;
        wo.shutdown().await
    };

    let server_to_client = async {
        let _ = copy(&mut ro, &mut wi).await?;
        wi.shutdown().await
    };

    tokio::try_join!(client_to_server, server_to_client)?;

    Ok(())
}
